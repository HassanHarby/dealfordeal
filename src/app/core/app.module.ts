import { BrowserModule } from '@angular/platform-browser';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';

import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SharedModule } from '../shared/Shared.module';
import { Configuration } from './configuration';
import {
  AppHeaderComponent,
  AppFooterComponent
} from '../layout';

const appLayout = [
  AppHeaderComponent,
  AppFooterComponent
];

@NgModule({
  declarations: [
    AppComponent,
    ...appLayout
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SharedModule.forRoot()
  ],
  providers: [Configuration, {
    provide: LocationStrategy,
    useClass: HashLocationStrategy
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }

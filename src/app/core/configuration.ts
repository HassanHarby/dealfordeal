import { Injectable } from '@angular/core';

@Injectable()
export class Configuration {
    HostUrl: string;

    constructor() {
        let local = {
            HostUrl: 'http://localhost:4200/api/'
        };
        // let local = {
        //     HostUrl: 'http://www.gsr-system-backend.gsr-sec.com:2082/api/'
        // };

        this.HostUrl = local.HostUrl;

    }
}


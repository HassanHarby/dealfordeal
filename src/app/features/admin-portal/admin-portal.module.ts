import { NgModule } from '@angular/core';
import { AdminPortalRoutingModule } from './admin-portal.routing';

@NgModule({
    imports: [
        AdminPortalRoutingModule,
    ],
})
export class AdminPortalModule { }

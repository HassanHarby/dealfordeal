import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { StorageService } from '../services/storage.service';

@Injectable()
export class AuthGuardService implements CanActivate {

    constructor(
        private router: Router,
        private storage: StorageService
    ) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) { 
        const token = this.storage.getToken();
        if (token) {
            return true;
        }

        this.router.navigate(['/account'], { queryParams: { returnUrl: state.url } });
        return false;

    }
}
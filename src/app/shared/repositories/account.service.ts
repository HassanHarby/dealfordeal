import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Configuration } from '../../core/configuration';

@Injectable()
export class AccountService {

	constructor(
		private http: HttpClient,
		private configuration: Configuration
	) { }

	hostUrl: string = this.configuration.HostUrl;
	apiUrlAccount: string = this.hostUrl + 'account/';


	login(model: any) {
		const url = this.apiUrlAccount + 'login';
		return this.http.post(url, model);
	}

	register(model: any) {
		const url = this.apiUrlAccount + 'register';
		return this.http.post(url, model);
	}

}

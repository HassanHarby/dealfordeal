import { Injectable } from '@angular/core';
import { StorageService } from './storage.service';

@Injectable()
export class AuthService {

	constructor(
		private storageService: StorageService
	) { }

	Logout() {
		this.storageService.removeToken();
		this.storageService.removeCurrentUser();
	}

}

import { Injectable } from '@angular/core';

@Injectable()
export class SharedDataService {
	EmailPattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	PasswordPattern = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[0-9a-zA-Z!@#$%^&*_+)(-]{8,}$/;

	LocalStorageEnum = {
		Lang: "Lang"
	};

	Roles = [
		{ Id: 1, Name: 'Admin' },
		{ Id: 2, Name: 'User' },
		{ Id: 3, Name: 'Gust' }
	];

}

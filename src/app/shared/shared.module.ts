import { NgModule, ModuleWithProviders } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { SharedDataService } from './services/shared-data.service';
import { AuthService } from './services/auth.service';
import { StorageService } from './services/storage.service';
import { AuthGuardService } from './guards/auth-guard.service';

import { AccountService } from './repositories/account.service';


@NgModule({
    imports: [
        FormsModule,
        CommonModule,
        RouterModule,
        HttpClientModule
    ],
    declarations: [],
    providers: [AccountService,
        StorageService, AuthGuardService
    ],

})
export class SharedModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: SharedModule,
            providers: [SharedDataService, AuthService, StorageService]
        };
    }
}
